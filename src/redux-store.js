import createStore from "redux/lib/createStore";

import applyMiddleware from "redux/lib/applyMiddleware";
import { createLogger } from "redux-logger";

import { persistStore, persistCombineReducers } from "redux-persist";
import storage from "redux-persist/lib/storage";

import { FLUSH } from "./constants";

import REDUCERS, { INITIAL } from "./reducers";

const combinedReducers = persistCombineReducers(
    {
        key: "store",
        storage,
    },
    REDUCERS
);

const store = createStore(
    (state, action) => {
        if (action.type === FLUSH) {
            return INITIAL;
        } else {
            return combinedReducers(state, action);
        }
    },
    INITIAL,
    applyMiddleware(
        createLogger({
            collapsed: true,
        })
    )
);
export const persistor = persistStore(store);

export default store;
