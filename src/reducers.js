import handleActions from "redux-actions/lib/handleActions";

import {
    CODE_FINDED,
    FETCH_ROUTERMAC_SUCCESS,
    FETCH_ROUTERMAC_REQUEST,
    FETCH_ROUTERMAC_FAILURE,
    PING_REQUEST_RUN,
    PING_REQUEST_FAILURE,
    PING_REQUEST_SUCCESS,
    FETCH_GOALSTATUS_REQUEST,
    FETCH_GOALSTATUS_SUCCESS,
    FETCH_GOALSTATUS_FAILURE,
} from "./constants";

const changeStateIfNonNull = (prevState, newState) =>
    prevState !== null ? newState : null;

const generateGUID = () => {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
        var r = (Math.random() * 16) | 0,
            v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
};


export const INITIAL = {
    GUID: generateGUID(),

    trackingNumber: null,
    // trackingNumber: "9405511899560836759691",
    type: null,
    // type: "qr",

    routerMacStatus: null,
    routerMac: null,

    pingStatus: null,
    goalStatus: null,
};



export default {
    GUID: GUID => GUID || INITIAL.GUID,

    trackingNumber: handleActions(
        {
            [CODE_FINDED]: (state, { payload }) => payload.trackingNumber,
        },
        INITIAL.trackingNumber
    ),

    type: handleActions(
        {
            [CODE_FINDED]: (state, { payload }) => payload.type,
        },
        INITIAL.type
    ),

    routerMacStatus: handleActions(
        {
            [FETCH_ROUTERMAC_REQUEST]: routerMacStatus => "loading",
            [FETCH_ROUTERMAC_SUCCESS]: routerMacStatus =>
                changeStateIfNonNull(routerMacStatus, "success"),
            [FETCH_ROUTERMAC_FAILURE]: routerMacStatus =>
                changeStateIfNonNull(routerMacStatus, "error"),
        },
        INITIAL.routerMacStatus
    ),

    routerMac: handleActions(
        {
            [FETCH_ROUTERMAC_SUCCESS]: (state, { payload }) =>
                payload.routerMac,
        },
        INITIAL.routerMac
    ),

    pingStatus: handleActions(
        {
            [PING_REQUEST_RUN]: pingStatus => "loading",
            [PING_REQUEST_SUCCESS]: pingStatus =>
                changeStateIfNonNull(pingStatus, "success"),
            [PING_REQUEST_FAILURE]: pingStatus =>
                changeStateIfNonNull(pingStatus, "error"),
        },
        INITIAL.pingStatus
    ),

    goalStatus: handleActions(
        {
            [FETCH_GOALSTATUS_REQUEST]: goalStatus => "loading",
            [FETCH_GOALSTATUS_SUCCESS]: goalStatus =>
                changeStateIfNonNull(goalStatus, "success"),
            [FETCH_GOALSTATUS_FAILURE]: goalStatus =>
                changeStateIfNonNull(goalStatus, "error"),
        },
        INITIAL.goalStatus
    ),
};
