import React from "react";
import { StyleSheet, Text, View, Button, TouchableOpacity, Dimensions } from "react-native";
import { BarCodeScanner, Permissions } from "expo";

const STYLES = {
    overlay: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
    },
    overlayScan: {
        position: "absolute",
        left: 0,
        right: 0,
        top: "7%",
        backgroundColor: "transparent",
    },
    overlayScanText: {
        textAlign: "center",
        fontSize: 22,
        color: "white",
        fontWeight: "bold",
    },
    overlayCancel: {
        position: "absolute",
        left: 0,
        right: 0,
        bottom: "7%",
        backgroundColor: "transparent",
    },
    overlayCancelLink: {
        color: "white",
        textAlign: "center",
        fontSize: 18,
        fontWeight: "bold",
    },
    square: {
        position: "absolute",
        backgroundColor: "rgba(0,0,0,.75)",
    },
    squareTop: {
        // height: "20%",
        top: 0,
        left: 0,
        right: 0,
    },
    squareBottom: {
        // height: "20%",
        bottom: 0,
        left: 0,
        right: 0,
    },
    squareLeft: {
        // width: "10%",
        // bottom: "20%",
        // top: "20%",
        left: 0,
    },
    squareRight: {
        // width: "10%",
        // bottom: "20%",
        // top: "20%",
        right: 0,
    },
};


export default class BarcodeScannerScreen extends React.Component {
    state = {
        hasCameraPermission: null,
        trackingNumber: null,
        type: null,
    };

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === "granted" });
    }

    static barCodeTypes = [
        BarCodeScanner.Constants.BarCodeType.code138,
        BarCodeScanner.Constants.BarCodeType.qr,
    ];

    render() {
        const { hasCameraPermission, trackingNumber, type } = this.state;

        const {width, height} = Dimensions.get("window");

        const squareLong = Math.min(width, height) * 0.8;
        const squareMargin = (height - squareLong) / 2;


        if (hasCameraPermission === null) {
            return <Text>Requesting for camera permission</Text>;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <View style={StyleSheet.absoluteFill}>
                    <BarCodeScanner
                        onBarCodeRead={this._handleBarCodeRead}
                        style={StyleSheet.absoluteFill}
                        barCodeTypes={BarcodeScannerScreen.barCodeTypes}
                    />

                    <View style={STYLES.overlay}>
                        <View style={[STYLES.square, STYLES.squareLeft, {
                            width: squareLong / 8,
                            bottom: squareMargin,
                            top: squareMargin,
                        }]} />
                        <View style={[STYLES.square, STYLES.squareRight, {
                            width: squareLong / 8,
                            bottom: squareMargin,
                            top: squareMargin,
                        }]} />

                        <View style={[STYLES.square, STYLES.squareTop, {
                            height: (height - squareLong) / 2
                        }]} />
                        <View style={[STYLES.square, STYLES.squareBottom, {
                            height: (height - squareLong) / 2
                        }]} />

                        <View style={STYLES.overlayScan}>
                            <Text style={STYLES.overlayScanText}>
                                Scan QR Code
                            </Text>
                        </View>

                        <View style={STYLES.overlayCancel}>
                            <TouchableOpacity
                                onPress={this.props.onReturnRequest}
                            >
                                <Text style={STYLES.overlayCancelLink}>
                                    Cancel
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            );
        }
    }

    _handleBarCodeRead = ({ type, data }) => {
        const normalizedTrackingCode = normalizeTrackingCode(data);

        if (normalizedTrackingCode) {
            console.log("finded: ", normalizedTrackingCode);
            this.setState(
                {
                    trackingNumber: normalizedTrackingCode,
                    type: {
                        [BarCodeScanner.Constants.BarCodeType.code138]: "bar",
                        [BarCodeScanner.Constants.BarCodeType.qr]: "qr",
                    }[type],
                },
                () => {
                    this.props.onTrackingNumberFinded(
                        this.state.trackingNumber,
                        this.state.type
                    );
                }
            );
        }
    };
}

const normalizeTrackingCode = str => {
    const normalizedStr = str.replace(/\D/g, "");
    if (normalizedStr.length < 22) {
        return null;
    }

    return normalizedStr;
    /*.slice(0, 22);*/
};

// const isDataIntNumberLike = (data) => {
//     return data.split('').every((letter) => {
//         return l === "" || l === " " || isIntNumberLike(l)
//     })
// };
// const isIntNumberLike = (str) => str === String(parseInt(str));
