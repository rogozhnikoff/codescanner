import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import BarcodeScanner from "./BarcodeScanner";
import { CODE_FINDED } from "../constants";

class Home extends React.Component {
    state = {
        scanMode: false,
    };

    onTrackingNumberFinded = (trackingNumber, type) => {
        this.props.dispatch({
            type: CODE_FINDED,
            payload: {
                trackingNumber,
                type,
            },
        });
    };
    onReturnRequest = () => {
        this.setState({ scanMode: false });
    };
    onScanRequest = () => {
        this.setState({ scanMode: true });
    };

    render() {
        const { scanMode } = this.state;

        if (scanMode) {
            return (
                <BarcodeScanner
                    onReturnRequest={this.onReturnRequest}
                    onTrackingNumberFinded={this.onTrackingNumberFinded}
                />
            );
        } else {
            return (
                <View>
                    <Text>Scan the shipment bar/qr code</Text>
                    <Button onPress={this.onScanRequest} title="scan" />
                    <Text>* it can ask you about permissions for camera</Text>
                </View>
            );
        }
    }
}

export default Home;
