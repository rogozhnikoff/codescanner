import React from "react";
import {
    StyleSheet,
    Text,
    View,
    Button,
    WebView,
    TouchableOpacity,
} from "react-native";
import { FLUSH } from "../constants";
import { persistor } from "../redux-store";
import PropTypes from "prop-types";
import _ from "lodash";

import * as API from "../api";

const STYLES = StyleSheet.create({
    wrapper: {
        width: "100%",
        padding: 10,
        // textAlign: "center"
    },
    webViewWrapper: {
        borderWidth: 2,
        borderColor: "indigo",
        marginTop: 42 + 25,
        width: "100%",
        height: "100%",
    },
    webViewHeader: {
        position: "absolute",
        top: -20,
        left: 0,
        right: 0,
    },
});

const fetchedStatuses = ["success", "loading", "error"];

class StatusScreen extends React.Component {
    static propTypes = {
        trackingNumber: PropTypes.string.isRequired,
        type: PropTypes.oneOf(["bar", "qr"]).isRequired,

        routerMacStatus: PropTypes.oneOf(fetchedStatuses),
        routerMac: PropTypes.string,

        GUID: PropTypes.string.isRequired,

        pingStatus: PropTypes.oneOf(fetchedStatuses),
        goalStatus: PropTypes.oneOf(fetchedStatuses),
    };

    constructor(props) {
        super(props);
        this.checkForRequests(props);
    }

    componentWillReceiveProps(newProps) {
        this.checkForRequests(newProps);
    }

    checkForRequests(props = this.props) {
        const {
            trackingNumber,
            routerMacStatus,
            routerMac,
            GUID,
            pingStatus,
            goalStatus,
        } = props;

        StatusScreen.delayInvokeBasedOnStatus(() => {
            API.getRouterMac(trackingNumber);
        }, routerMacStatus);

        if (routerMacStatus === "success") {
            StatusScreen.delayInvokeBasedOnStatus(() => {
                API.ping(routerMac, GUID);
            }, pingStatus);

            if (pingStatus === "success") {
                StatusScreen.delayInvokeBasedOnStatus(() => {
                    API.checkGoalStatus(routerMac, GUID);
                }, goalStatus);
            }
        }
    }

    static delayInvokeBasedOnStatus = (f, status) => {
        if (status === null) {
            f();
        } else if (status === "error") {
            setTimeout(() => {
                f();
            }, 10000);
        }
    };

    render() {
        const {
            trackingNumber,
            type,
            routerMacStatus,
            routerMac,
            pingStatus,
            goalStatus,
            GUID,
        } = this.props;

        if (goalStatus === "success") {
            return (
                <View style={STYLES.webViewWrapper}>
                    <View style={STYLES.webViewHeader}>
                        <Text>it's a WebView, baby</Text>
                        <View
                            style={{ position: "absolute", right: 0, top: 0 }}
                        >
                            <TouchableOpacity onPress={this.onRemoveRequest}>
                                <Text>remove code and restart</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <WebView
                        style={STYLES.webView}
                        source={{
                            uri: `https://settings.cleanrouter.com?mac=${routerMac}`,
                        }}
                    />
                </View>
            );
        }

        return (
            <View style={STYLES.wrapper}>
                <Text>
                    Well, we've finded {type} postal code: {"\n"}
                </Text>
                <Text>{trackingNumber}</Text>

                <Text>statuses:</Text>
                <View>
                    <Text>
                        <Status status={routerMacStatus} /> Getting Router Mac:{" "}
                        {routerMac}
                    </Text>
                    <Text>
                        <Status status={pingStatus} />
                        <StatusHttp
                            status={pingStatus}
                            render={() => <Text>Ping: {pingStatus}</Text>}
                            throubleRender={() => (
                                <Text>Troubleshooting info goes here</Text>
                            )}
                        />
                    </Text>
                    <Text>
                        <Status status={goalStatus} />
                        <StatusHttp
                            status={pingStatus}
                            render={() => <Text>Goal status: {goalStatus}</Text>}
                            throubleRender={() => (
                                <Text>Troubleshooting info goes here</Text>
                            )}
                        />

                    </Text>
                </View>
                <View style={{ marginTop: 25 }}>
                    <Text>GUID: {GUID}</Text>
                </View>
                <View style={{ marginTop: 25 }}>
                    <Button
                        title="remove code and restart"
                        onPress={this.onRemoveRequest}
                    />
                </View>
            </View>
        );
    }

    onRemoveRequest = () => {
        this.props.dispatch({
            type: FLUSH,
        });
        persistor.purge();
    };
}

class StatusHttp extends React.Component {
    state = {
        trouble: false,
    };

    constructor(props) {
        super(props);
        this.timeout = null;
        this.checkForLoading(props);
    }
    componentWillReceiveProps(newProps) {
        this.checkForLoading(newProps);
    }

    checkForLoading(props = this.props) {
        if (props.status === "loading" && _.isNil(this.timeout)) {
            this.timeout = setTimeout(() => {
                this.setState({
                    trouble: true,
                });
            }, 3000);
        }
        if (props.status !== "loading" && !_.isNil(this.timeout)) {
            clearTimeout(this.timeout);

            if (this.state.trouble) {
                this.setState({
                    trouble: false,
                });
            }
        }
    }
    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    render() {
        const { status } = this.props;
        const { trouble } = this.state;

        if (status === "loading" && trouble) {
            return this.props.throubleRender(this.props, this.state);
        } else {
            return this.props.render(this.props, this.state);
        }
        //
        // return (
        //     <Text>
        //         {status === null && "..."}
        //         {status === "success" && "✔︎"}
        //         {status === "loading" && !trouble && "❂︎"}
        //         {status === "loading" && trouble && "⌧"}
        //         {status === "error" && "✖︎"}
        //     </Text>
        // );
    }
}

const Status = props => {
    const { status } = props;
    return (
        <Text>
            {status === null && "..."}
            {status === "success" && "✔︎"}
            {status === "loading" && "❂︎"}
            {status === "error" && "✖︎"}
        </Text>
    );
};

export default StatusScreen;
