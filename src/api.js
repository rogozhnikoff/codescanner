import store from "./redux-store";
import reduce from "lodash/reduce";
import debounce from "lodash/debounce";

makeDebounce = f =>
    debounce(f, 50, {
        trailing: false,
        leading: true,
    });

// ----------------------------------------------------------------------------------
import {
    FETCH_ROUTERMAC_SUCCESS,
    FETCH_ROUTERMAC_REQUEST,
    FETCH_ROUTERMAC_FAILURE,
} from "./constants";

const isValidRouterMac = str => {
    return str.split(":").length === 6;
};

export const getRouterMac = makeDebounce(code => {
    store.dispatch({ type: FETCH_ROUTERMAC_REQUEST });

    return fetch(
        "http://update.pandorashope.net/ws/REST.handler?m=GetRouterMac",
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                trackingNumber: code,
            }),
        }
    )
        .then(
            res => {
                return res.text();
            },
            err => store.dispatch({ type: FETCH_ROUTERMAC_FAILURE, error: err })
        )
        .then(routerMac => {
            if (isValidRouterMac(routerMac)) {
                store.dispatch({
                    type: FETCH_ROUTERMAC_SUCCESS,
                    payload: { routerMac },
                });
            } else {
                store.dispatch({
                    type: FETCH_ROUTERMAC_FAILURE,
                    error: {
                        routerMac,
                    },
                });
            }
            return routerMac;
        });
});
// ----------------------------------------------------------------------------------
import {
    PING_REQUEST_RUN,
    PING_REQUEST_FAILURE,
    PING_REQUEST_SUCCESS,
} from "./constants";

const toSearchString = hash =>
    reduce(hash, (memo, value, key) => memo + `${key}=${value}&`, "?");

export const ping = makeDebounce((routerMac, GUID) => {
    store.dispatch({ type: PING_REQUEST_RUN });

    const url =
        "https://remote.cleanrouter.com/senddata.php" +
        toSearchString({
            toID: routerMac, // String_Returned_From_4_above
            fromID: GUID, // Generated_GUID_from_5_above
            cmd: "ping",
            session: "550e8400e29b41d4a716446655440000",
        });

    return fetch(url, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then(
            res => {
                return res.text();
            },
            err => {
                store.dispatch({ type: PING_REQUEST_FAILURE });
            }
        )
        .then(status => {
            if (status === "success") {
                store.dispatch({
                    type: PING_REQUEST_SUCCESS,
                });
            } else {
                store.dispatch({
                    type: PING_REQUEST_FAILURE,
                    error: {
                        status,
                    },
                });
            }
        });
});

// ----------------------------------------------------------------------------------
import {
    FETCH_GOALSTATUS_REQUEST,
    FETCH_GOALSTATUS_FAILURE,
    FETCH_GOALSTATUS_SUCCESS,
} from "./constants";

export const checkGoalStatus = makeDebounce((routerMac, GUID) => {
    store.dispatch({ type: FETCH_GOALSTATUS_REQUEST });

    const url =
        "https://remote.cleanrouter.com/getdata.php?" +
        toSearchString({
            toID: GUID, // Generated_GUID_from_5_above
            fromID: routerMac, // String_Returned_From_4_above
            session: "550e8400e29b41d4a716446655440000",
        });

    return fetch(url, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then(
            res => {
                return res.json();
            },
            err => {
                store.dispatch({ type: FETCH_GOALSTATUS_FAILURE, error: err });
            }
        )
        .then(response => {
            const { result, status } = response;

            if (status === "success") {
                store.dispatch({
                    type: FETCH_GOALSTATUS_SUCCESS,
                });
            } else {
                store.dispatch({
                    type: FETCH_GOALSTATUS_FAILURE,
                    response: response,
                });
            }
        });
});
