import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import store, {persistor} from "./src/redux-store";
import {Provider, connect} from "react-redux";
import HomeScreen from "./src/screen/Home";
import StatusScreen from "./src/screen/StatusScreen";
import { PersistGate } from 'redux-persist/lib/integration/react';

const STYLES = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'center',
        width: "100%",
    },
});



class App extends React.Component {
    render() {
        const {trackingNumber} = this.props;

        return (
            <View style={STYLES.container}>
                {!trackingNumber && <HomeScreen {...this.props} />}
                {trackingNumber && <StatusScreen {...this.props} />}
            </View>
        );
    }
}

const AppConnected = connect(
    (state) => state
)(App);


export default () => (<Provider store={store}>
    <PersistGate persistor={persistor}>
        <AppConnected/>
    </PersistGate>
</Provider>);