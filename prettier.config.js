module.exports = {
    useTabs: false,
    printWidth: 80,
    tabWidth: 4,
    singleQuote: false,
    bracketSpacing: true,
    trailingComma: "es5",
    jsxBracketSameLine: false,
    parser: "flow",
    semi: true,
};
